'use strict';

const auth = require('../controllers/auth.js');
const billController = require('../controllers/bill.js');

module.exports = app => {
    app.route('/api/bill/new')
        .all(auth.isLoggedIn)
    // Create a bill
        .post(billController.create);

    app.route('/api/bill/brief')
        .all(auth.isLoggedIn)
    // Get bills’ summary
        .get(billController.getBrief)

    app.route('/api/bill/:billNumber')
        .all(auth.isLoggedIn)
    // Get bill detail
        .get(billController.get)
        .post(billController.edit);

    app.route('/api/bill/:billNumber/delete')
        .all(auth.isLoggedIn)
    // Get bill detail
        .get(billController.deleteBill);

    app.route('/api/bill/:billNumber/service/new')
        .all(auth.isLoggedIn)
    // Add bill service
        .post(billController.postService);

    app.route('/api/bill/:billNumber/service/delete')
        .all(auth.isLoggedIn)
    // Add bill service
        .post(billController.deleteService);

    app.route('/api/bill/:billNumber/pdf')
        .all(auth.isLoggedIn)
    // Get bill PDF version
        .get(billController.getPDF);

    app.route('/api/bill/:billNumber/pdf/generate')
        .all(auth.isLoggedIn)
    // Get bill PDF version
        .get(billController.generatePDF);
};
