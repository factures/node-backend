'use strict';

const { Sequelize, DataTypes, Model } = require('sequelize');

class Service extends Model {
    getAmount() {
        let diff = Math.abs(this.endingDate.getTime() - this.startingDate.getTime());
        let nbOfDays = Math.ceil(diff / (1000 * 3600 * 24));
        return nbOfDays * this.dailyTariff;
    }
}

module.exports = (sequelize, DataTypes) => {
    Service.init({
        label: {
            type: DataTypes.STRING,
            allowNull: false
        },
        startingDate: {
            type: DataTypes.DATE,
            allowNull: false
        },
        endingDate: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dailyTariff: {
            type: DataTypes.INTEGER,
        }
    }, {sequelize, modelName: 'services'});

    Service.associate = function(models) {
        Service.belongsTo(models['bills']);
    };

    return Service;
};
