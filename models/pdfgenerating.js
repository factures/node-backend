'use strict';

const { Sequelize, DataTypes, Model } = require('sequelize');

class PDFGenerating extends Model {

}

module.exports = (sequelize, DataTypes) => {
    PDFGenerating.init({}, {
        sequelize,
        modelName: 'pdfgenerating',
        freezeTableName: true
    });

    PDFGenerating.associate = function(models) {
        PDFGenerating.belongsTo(models['bills']);
    };

    return PDFGenerating;
};
