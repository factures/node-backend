'use strict';

const { Sequelize, DataTypes, Model } = require('sequelize');

class Bill extends Model {
    static getBrief(userId) {
        return this.findAll({
            attributes: {
                exclude: [
                    'createdAt',
                    'updatedAt',
                    'PDFPath'
                ]
            },
            where: {
                userId: userId
            }
        }).then(async (bills) => {
            let brief = [];
            for (let i = 0; i < bills.length; i++) {
                brief[i] = bills[i].toJSON();
                brief[i].amount = await bills[i].getAmount();
                delete brief[i].id;
                delete brief[i].userId;
            }

            return brief;
        });
    }

    getAmount() {
        return this.getServices().then((services) => {
            if (services.length > 0) {
                return services.map(service => service.getAmount())
                    .reduce((total, amount) => total + amount);
            } else {
                return 0;
            }
        });
    }
}

module.exports = (sequelize, DataTypes) => {
    Bill.init({
        number: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        billingAddressL1: {
            type: DataTypes.STRING,
            allowNull: false
        },
        billingAddressL2: {
            type: DataTypes.STRING,
            allowNull: false
        },
        billingAddressL3: {
            type: DataTypes.STRING,
            allowNull: false
        },
        PDFGenerated: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        PDFPath: {
            type: DataTypes.STRING
        }
    }, {sequelize, modelName: 'bills'});

    Bill.associate = function(models) {
        Bill.belongsTo(models['users']);
        Bill.hasMany(models['services'], {
            hooks: true,
            onDelete: 'CASCADE'
        });
        Bill.hasOne(models['pdfgenerating'], {
            hooks: true,
            onDelete: 'CASCADE'
        });
    };

    return Bill;
};
