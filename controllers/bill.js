const Bill = require('../models/index.js')['bills'];
const Service = require('../models/index.js')['services'];
const PDFGenerating = require('../models/index.js')['pdfgenerating'];
const path = require('path');
const child_process = require('child_process');

module.exports = {
    getBrief: (req, res) => {
        // Brief format :
        //
        // {
        //   '1': {  // the key is the bill’s number
        //     'amount': '100',
        //     'to': 'Company Name'
        //   },
        //   ...
        // }
        Bill.getBrief(req.user.id).then(brief => {
            res.status(200).json(brief);
        }).catch((error) => {
            console.log('Get brief error');
            console.log(error);
            res.status(400).end();
        });
    },

    create: (req, res) => {
        req.body.userId = req.user.id;
        req.body.PDFGenerated = false;
        Bill.create(req.body)
            .then(bill => {
                res.status(200).json({ 'message': 'Bill created.' });
            }).catch((error) => {
                console.log('Get bill error');
                console.log(error);
                res.status(400).end();
            });
    },

    get: (req, res) => {
        Bill.findOne({
            attributes: {
                exclude: [
                    'id',
                    'userId',
                    'createdAt',
                    'updatedAt',
                    'PDFPath',
                    'services.billId',
                    'services.createdAt',
                    'services.updatedAt'
                ]
            },
            where: {
                userId: req.user.id,
                number: req.params.billNumber
            },
            include: [
                { model: Service }
            ]
        }).then(bill => {
            res.status(200).json(bill.toJSON());
        }).catch(error => {
            console.log(error);
            res.status(400).json({ 'message': error });
        });
    },

    edit: (req, res) => {
        req.body.userId = req.user.id;
        req.body.PDFGenerated = false;
        delete req.body.number;
        Bill.update(req.body, {
            where: {
                userId: req.user.id,
                number: req.params.billNumber
            }
        }).then(() => {
            res.status(200).json({ 'message': 'Bill updated.' });
        }).catch(() => {
            res.status(400).json({ 'message': 'Error while updating bill.' });
        });
    },

    postService: (req, res) => {
        Bill.findOne({
            where: {
                userId: req.user.id,
                number: req.params.billNumber
            }
        }).then(bill => {
            bill.PDFGenerated = false;
            return bill.save().then(bill => {
                return bill.createService(req.body);
            });
        }).then(service => {
            res.status(200).json({ 'message': 'Service created.' });
        }).catch((error) => {
            console.log('Service not created: ', error);
            res.status(400).json({ 'message': 'Service not created'});
        });
    },

    deleteService: (req, res) => {
        Bill.findOne({
            where: {
                userId: req.user.id,
                number: req.params.billNumber
            }
        }).then(bill => {
            bill.PDFGenerated = false;
            return bill.sync();
        }).then(bill => {
            return Service.destroy({
                where: {
                    id: req.body.id,
                    billId: bill.id
                }
            });
        }).then(() => {
                res.status(200).json({ 'message': 'Service deleted.' });
        }).catch((error) => {
            res.status(400).json({ 'message': 'Error, service not deleted.' });
        });
    },

    deleteBill: (req, res) => {
        Bill.destroy({
            where: {
                number: req.params.billNumber,
                userId: req.user.id
            }
        }).then(bill => {
            res.status(200).json({ 'message': 'Bill #' + req.params.billNumber + ' deleted.' });
        }).catch(error => {
            console.log(error);
            res.status(400).json({ 'message': 'Bill #' + req.params.billNumber + ' has not been deleted.' });
        });
    },

    generatePDF: (req, res) => {
        Bill.findOne({
            include: Service,
            where: {
                number: req.params.billNumber,
                userId: req.user.id
            }
        }).then(bill => {
            if (bill.PDFGenerated) {
                res.status(200).json({ message: 'PDF has already been generated.' });
            } else if (bill.getServices().length <= 0) {
                res.status(500).json({ message: 'Can’t generate empty bill.' });
            } else {
                PDFGenerating.findOne({
                    where: {
                        billId: bill.id
                    }
                }).then(pdfgenerating => {
                    if (pdfgenerating) {
                        console.log('PDF is already being generated');
                        console.log(pdfgenerating);
                        res.status(200).json({
                            message: 'PDF is already being generated'
                        });
                    } else {
                        PDFGenerating.create({ billId: bill.id })
                            .then(pdfgenerating => {
                                let c = child_process.exec('/usr/local/bin/python3.8 generate.py', {
                                    cwd: 'data/bills/',
                                    windowsHide: true
                                }, async (error, stdout, stderr) => {
                                    console.log('STDOUT: ', stdout);
                                    console.log('STDERR: ', stderr);
                                    const log = JSON.parse(stdout);
                                    console.log('Le journal:');
                                    console.log(log);
                                    if (!log.error && log.path) {
                                        await PDFGenerating.destroy({
                                            where: {
                                                billId: bill.id
                                            }
                                        });

                                        await Bill.update({
                                            PDFGenerated: true,
                                            PDFPath: log.path
                                        }, {
                                            where: {
                                                userId: req.user.id,
                                                number: req.params.billNumber
                                            }
                                        });
                                    } else {
                                        console.log('log.error || !log.path');
                                    }
                                });

                                let billInfo = {
                                    'user.id': req.user.id,
                                    'user.name': req.user.companyName,
                                    'user.company.number': req.user.companyNumber,
                                    'user.company.address': [req.user.companyAddress, req.user.companyAddress, req.user.companyAddress],
                                    'user.phoneNumber': req.user.phoneNumber,
                                    'user.email': req.user.email,
                                    'number': bill.number,
                                    'billingAddress': [bill.billingAddressL1, bill.billingAddressL2, bill.billingAddressL3]
                                };

                                billInfo.lines = bill.services.map(service => {
                                    return {
                                        'label': service.label,
                                        'dailyTariff': service.dailyTariff,
                                        'startingDate': service.startingDate,
                                        'endingDate': service.endingDate
                                    };
                                });

                                console.log('Bill info: ');
                                console.log(JSON.stringify(billInfo));

                                c.stdin.write(JSON.stringify(billInfo));

                                c.stdin.end();

                                res.status(200).json({ message: 'Bill is generating.' });
                            }).catch(error => {
                                console.log(error);
                                res.status(500).json( { message: 'Can’t generate PDF.' });
                            });
                    }
                });
            }
        }).catch(error => {
            res.status(400).json({ message: 'Bill#' + req.params.billNumber + ' doesn’t exists.' });
        });
    },

    getPDF: (req, res) => {
        Bill.findOne({
            where: {
                userId: req.user.id,
                number: req.params.billNumber
            }
        }).then(bill => {
            if(!bill.PDFGenerated) {
                res.status(500).json({ 'message': 'The PDF has not been generated yet.' });
            } else {
                res.download(bill.PDFPath, path.basename(bill.PDFPath), err => {
                    res.status(400).json({ 'message': 'Can’t send bill.' });
                });
            }
        }).catch(err => {
            res.status(400).json({ 'message': 'Can’t find bill.' });
        });
    }
};
