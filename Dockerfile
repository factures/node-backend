FROM debian:stretch as python

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y curl tar build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget

WORKDIR /usr/src/python

RUN curl -O https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tar.xz \
        && tar -Jxf Python-3.8.1.tar.xz && rm Python-3.8.1.tar.xz && cd Python-3.8.1/ \
        && ./configure --enable-optimizations && make -j5

FROM node:13.6.0

COPY --from=python /usr/src/python /usr/src/python
WORKDIR /usr/src/python/Python-3.8.1
RUN make altinstall
RUN rm -fr /usr/src/python

# To generate the latex bill.
RUN apt-get update
RUN apt-get install -y texlive-full locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8
ENV LANG en_US.UTF-8 
RUN pip3.8 install jsonschema

COPY ./docker/usr/bin/docker-entrypoint.sh /usr/bin/docker-entrypoint.sh
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /usr/bin/wait-for-it.sh
RUN chmod +x /usr/bin/wait-for-it.sh

WORKDIR /usr/src/app/data/bills
RUN wget https://framagit.org/factures/latexbillgenerator/-/archive/master/latexbillgenerator-master.tar.bz2 && tar -jxf latexbillgenerator-master.tar.bz2 && mv latexbillgenerator-master/* . && rmdir latexbillgenerator-master && rm latexbillgenerator-master.tar.bz2

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
# If building for production
# RUN npm ci --only=production

# Bundle app source code
COPY . .

EXPOSE 3000

ENTRYPOINT ["/bin/sh", "-c"]
CMD ["/usr/bin/wait-for-it.sh postgres:5432 -- /usr/bin/docker-entrypoint.sh"]
