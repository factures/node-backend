'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('pdfgenerating', {
          id: {
              allowNull: false,
              autoIncrement: true,
              primaryKey: true,
              type: Sequelize.INTEGER
          },
          billId: {
              type: Sequelize.INTEGER,
              allowNull: false,
              onUpdate: 'cascade',
              onDelete: 'cascade',
              references: {
                  model: {
                      tableName: 'bills'
                  }
              }
          },
          createdAt: {
              allowNull: false,
              type: Sequelize.DATE
          },
          updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
          }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('pdfgenerating');
  }
};
