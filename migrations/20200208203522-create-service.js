'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('services', {
          id: {
              allowNull: false,
              autoIncrement: true,
              primaryKey: true,
              type: Sequelize.INTEGER
          },
          billId: {
              type: Sequelize.INTEGER,
              allowNull: false,
              onUpdate: 'cascade',
              onDelete: 'cascade',
              references: {
                  model: {
                      tableName: 'bills'
                  }
              }
          },
          label: {
              type: Sequelize.STRING,
              allowNull: false
          },
          startingDate: {
              type: Sequelize.DATE,
              allowNull: false
          },
          endingDate: {
              type: Sequelize.DATE,
              allowNull: false
          },
          dailyTariff: {
              type: Sequelize.INTEGER,
          },
          createdAt: {
              allowNull: false,
              type: Sequelize.DATE
          },
          updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
          }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('services');
  }
};
