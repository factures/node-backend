'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('bills', {
          id: {
              allowNull: false,
              autoIncrement: true,
              primaryKey: true,
              type: Sequelize.INTEGER
          },
          userId: {
              type: Sequelize.INTEGER,
              allowNull: false,
              onUpdate: 'cascade',
              onDelete: 'cascade',
              references: {
                  model: {
                      tableName: 'users'
                  }
              }
          },
          number: {
              type: Sequelize.INTEGER,
              allowNull: false
          },
          date: {
              type: Sequelize.DATE,
              allowNull: false
          },
          billingAddressL1: {
              type: Sequelize.STRING,
              allowNull: false
          },
          billingAddressL2: {
              type: Sequelize.STRING,
              allowNull: false
          },
          billingAddressL3: {
              type: Sequelize.STRING,
              allowNull: false
          },
          PDFGenerated: {
              type: Sequelize.BOOLEAN,
              allowNull: false,
              defaultValue: false
          },
          PDFPath: {
              type: Sequelize.STRING
          },
          createdAt: {
              allowNull: false,
              type: Sequelize.DATE
          },
          updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
          }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('bills');
  }
};
